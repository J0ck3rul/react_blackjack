import React, { useContext } from 'react';


export function Deck() {

  const context = useContext();

  return <div>
    Number of cards on the deck: {context.cardsCount}
    </div>;
}
