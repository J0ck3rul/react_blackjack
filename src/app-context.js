import * as React from 'react';

const AppContext = React.createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case 'decrement': {
      return { ...state, count: state.count - 1 }
    }
    default: {
      throw new Error('no action type');
    }
  }
}

const provider = ({children}) => {
  const [state, dispatch] = React.useReducer(reducer, {count: 0});

  const value = {state, dispatch};

  return 
}