import React from 'react';
import './App.css';
import { Deck } from './components';

let state = {
  cardsCount: 52,
  players: [
    {
      name: "first player",
      cardsCount: 2,
      cards: []
    },
    {
      firstPlayer: "second player",
      cardsCount: 2,
      cards: []
    }
  ]
}

export const AppContext = React.createContext(state);



function App() {

  const drawCard = () => {
    state = {...state, cardsCount: state.cardsCount-1}
  }
  return (
    <AppContext.Provider value="blackjack">
      application is here
      {/* <Player></Player> */}
      <Deck></Deck>
      <button onClick={e => drawCard()}>draw</button>
    </AppContext.Provider>
  );
}

export default App;
